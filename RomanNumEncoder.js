// Symbol    Value
// I          1
// V          5
// X          10
// L          50
// C          100
// D          500
// M          1,000
function solution(number){
  var keyMap = {'M':1000,'CM': 900,'D':500,'CD':400,'C':100,'XC':90,'L':50,'XL':40,'X':10,'IX':9,'V':5,'IV':4,'I':1};
  var result = '';
  for (i in keyMap){
    console.log('NumberLeft:' + number);
    console.log('MapValue:' + keyMap[i]);

    if (number >= keyMap[i]) {
      number -= keyMap[i];
      result += i;
    }
    console.log('Roman Value:'+ result);
    console.log('--------');
  }
  return result;
}

console.log(solution(2008));
