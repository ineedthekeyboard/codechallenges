function formatDuration(seconds) {
    //Calculate Units
    if (seconds < 1){
        return 'now';
    }
    year = (60*60*24*365);
    day = (24 * 60 * 60);
    hour = (60 * 60);
    min = 60;
    nums = {
        year:  Math.floor(seconds/year),
        day: Math.floor((seconds - Math.floor(seconds/year)* year)/day),
        hour: Math.floor((seconds - Math.floor(seconds/day)* day)/hour),
        minute: Math.floor((seconds - Math.floor(seconds/hour)* hour)/min) % 60,
        second: seconds % 60
    };
    var answer = [], timeBlock;
    for(var obj in nums) {
        timeBlock = obj;
        if (nums[obj] > 0) {
            if(nums[obj] > 1) {
                timeBlock += 's';
            }
            answer.push(nums[obj].toString());
            answer.push(timeBlock);
        }
    }
    for (var i = 0; i < answer.length; i++) {
        if (i < answer.length - 3 && i%2 !== 0){
            answer[i] += ','
        }
        if (i === answer.length - 3 && answer.length > 3){
            answer[i] += ' and';
        }
    }

    return answer.join(' ');
}
//pass in the number of seconds to convert
console.log(formatDuration(15731080));
