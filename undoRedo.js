function undoRedo(object) {
  var specialObj = object;
  var redoObj = {
    isPossible: false,
    operation: null,
    prevValue: null
  };
  var undoObj = {
    newUndo: true,
    isPossible: true,
    operation: null,
    prevValue: null
  };
	return {

    //todo is Undoable
		set: function(key, value) {
      if (specialObj.hasOwnProperty(key)) {
        //update the key and set the undo property
        undoObj.operation = 'update';
        undoObj.prevKey = key;
        undoObj.prevValue = specialObj[key];
        undoObj.isPossible = true;
        specialObj[key] = value;
      } else {
        //create the key
        undoObj.operation = 'create';
        undoObj.prevKey = null;
        undoObj.prevValue = null;
        undoObj.isPossible = true;
        specialObj[key] = value;
      }
    },
		get: function(key) {
      return specialObj[key];
    },

    //todo is Undoable
		del: function(key) {
      if (typeof specialObj[key] != "undefined") {
        undoObj.operation = 'delete';
        undoObj.prevKey = key;
        undoObj.prevValue = specialObj[key];
        undoObj.isPossible = true;
        delete specialObj[key];
      }
    },

    //can only undo a set/del operation
		undo: function() {
      // if (!undoObj.isPossible) {
      //   return;
      // }
      if (undoObj.newUndo) {
        if (undoObj.operation === 'update' || undoObj.operation === 'delete') {
          specialObj[undoObj.prevKey] = undoObj.prevValue;
          undoObj.newUndo = false;
        }
        if (undoObj.operation === 'create' && specialObj.hasOwnProperty(undoObj.prevKey)) {
          delete specialObj[undoObj.prevKey];
          undoObj.newUndo = false;
        }
      }
    },

    //can only happen after a undo of set/del operation
		redo: function() {}
	};
}
//************************
//Test Cases:
var obj = {
  x: 1,
  y: 2
};

var unRe = undoRedo(obj);

console.log(unRe.get('x'));
unRe.set('x', 3);
console.log(unRe.get('x'));
